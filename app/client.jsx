import React from 'react';
import { render as domRender } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import config from './config/client';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { rehydrate } from './store/client';

import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import I18nextProvider from 'react-i18next/dist/commonjs/I18nextProvider';
import locales from './locales';

import App from './app';

// STYLES

import './styles/index.css';

// CONTENT

function initialRender() {
	const store = rehydrate(window.__INITIAL_STATE__);
	const rootNode = document.getElementById('root');

	// LANG
	var lang = config.DEFAULT_LANGUAGE;
	if(location.pathname.indexOf('/en') == 0) lang = 'en';
	else if(location.pathname.indexOf('/de') == 0) lang = 'de';

	i18n.init({
      lng: lang,
      fallbackLng: false,
      returnEmptyString: false,
      keySeparator: false,
      nsSeparator: false,
      resources: locales
    });

	// MOUNT
	const renderApp = AppComponent => {
		var app;
		if (config.DEBUG) {
			app = <AppContainer>
				<I18nextProvider i18n={ i18n }>
					<Provider store={store}>
						<BrowserRouter>
							<AppComponent />
						</BrowserRouter>
					</Provider>
				</I18nextProvider>
			</AppContainer>
		}
		else {
			app = <I18nextProvider i18n={ i18n }>
				<Provider store={store}>
					<BrowserRouter>
						<AppComponent />
					</BrowserRouter>
				</Provider>
			</I18nextProvider>
		}
		domRender(app, rootNode);
	};

	renderApp(App);

	// Hot Module Replacement API
	if (module.hot) {
		module.hot.accept('./app', () => {
			renderApp(App)
		});
	}

	// Dispatch *after* the server generated state has been consumed
	store.dispatch({ type: 'RESTORE' });
}


// INIT
initialRender();
