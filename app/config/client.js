module.exports = {
  DEBUG: true,
  APP_NAME: 'Servius',
  SERVER_URL: 'https://www.servius.es',
  GOOGLE_ANALYTICS_CODE: '',
	SUPPORTED_LANGUAGES: ['es', 'en', 'de'],
	DEFAULT_LANGUAGE: 'es'
};
