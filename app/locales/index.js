var config = require('../config/client');

// Exported object will be like:
//
// {
//   en: {translation: { ... }}
//   fr: {translation: { ... }}
// };

module.exports = config.SUPPORTED_LANGUAGES.reduce(function(locales, lang){
  try {
    locales[lang] = {translation: require('./' + lang + '/translation.json')};
  }
  catch(e) {
    console.error("ERROR: The translation file is not available for", lang, "\nMake sure that you have generated the language templates by running \"npm run po:extract\"\n");
  }

  return locales;
}, {});
