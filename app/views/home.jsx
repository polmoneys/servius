import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { translate } from "react-i18next";
import Waypoint from 'react-waypoint';
import Media from 'react-media';
import { Carousel } from 'react-responsive-carousel';
import scrollToComponent from 'react-scroll-to-component';
import { slide as Menu } from 'react-burger-menu';

@translate()
@connect(({ app }) => ({ app }))
class Home extends Component {
    static propTypes = {
        app: PropTypes.object.isRequired,
        t: PropTypes.func.isRequired
    }


    state = {
        first: false,
        cubes: false,
        second: false,
        third: false,
        fourth: false,
        fifth: false,
        sixth: false,
        visible: true
    }


    render() {
        const first = this.state.first,
            cubes = this.state.cubes,
            second = this.state.second,
            third = this.state.third,
            fourth = this.state.fourth,
            fifth = this.state.fifth,
            sixth = this.state.sixth,
            visible = this.state.visible;

        const { t } = this.props;

        let images = [

            { _id: 1, title: "D", url: '../media/carrusel-1.jpg' },
            { _id: 2, title: "Interiores", url: '../media/carrusel-2.jpg' },
            { _id: 3, title: "Obra Civil", url: '../media/carrusel-3.jpg' },
            { _id: 4, title: "Locales Comerciales", url: '../media/carrusel-4.jpg' },
            { _id: 4, title: "Diseño y Legalizaciones", url: '../media/carrusel-1.jpg' },
            { _id: 5, title: "Instalaciones", url: '../media/carrusel-5.jpg' }
        ]


        return (
            <div id="home">




                <Media query="(min-width: 700px)" render={() => (
                    <div className={this.state.visible ? "barra-nav" : 'barra-nav barra-hidden'}>

                        <ul className="nav">
                            <li onClick={() => scrollToComponent(this.Intro, { offset: -120, align: 'top', duration: 1500 })}>{t("Conócenos")}</li>
                            <li onClick={() => scrollToComponent(this.Inmobiliaria, { offset: -15, align: 'top', duration: 1500 })}> {t("Sector inmobiliario B2B")}</li>
                            <li onClick={() => scrollToComponent(this.Empresas, { offset: -4, align: 'top', duration: 1500 })}> {t("Empresas")}</li>
                            <li onClick={() => scrollToComponent(this.Administracion, { offset: -10, align: 'top', duration: 1500 })}> {t("Administración Pública")}</li>
                            <li onClick={() => scrollToComponent(this.Particulares, { offset: -10, align: 'top', duration: 1500 })}> {t("Particulares")}</li>
                            <li onClick={() => scrollToComponent(this.Contacta, { offset: -55, align: 'top', duration: 1500 })}> {t("Contacta")}</li>
                        </ul>
                    </div>
                )} />


                <Media query="(max-width: 700px)" render={() => (
                    <div className="burger">
                        <Menu right isOpen={false} noOverlay>
                            <ul className="nav">
                                <li onClick={() => scrollToComponent(this.Intro, { offset: 0, align: 'top', duration: 1500 })}> {t("Conócenos")}</li>
                                <li onClick={() => scrollToComponent(this.Inmobiliaria, { offset: 0, align: 'top', duration: 1500 })}> {t("Inmobiliaria y Promotoras")}</li>
                                <li onClick={() => scrollToComponent(this.Empresas, { offset: 0, align: 'top', duration: 1500 })}> {t("Empresas")}</li>
                                <li onClick={() => scrollToComponent(this.Administracion, { offset: 0, align: 'top', duration: 1500 })}> {t("Administración Pública")}</li>
                                <li onClick={() => scrollToComponent(this.Particulares, { offset: 0, align: 'top', duration: 1500 })}> {t("Particulares")}</li>
                                <li onClick={() => scrollToComponent(this.Contacta, { offset: 0, align: 'top', duration: 1500 })}> {t("Contacta")}</li>
                            </ul>
                        </Menu>

                    </div>
                )} />






                <Carousel showStatus={false} useKeyboardArrows={true} showArrows={true} showThumbs={false}>
                    {
                        images.map((image) =>
                            (
                                <div className="img-ctn" key={image._id}>
                                    {(image._id == 1) ? <img src="../media/logo-servius.png" className="logo-banner" /> : <p className="img-title">{image.title}</p>}
                                    <img src={image.url} alt={image.title} className="img-responsive" />
                                </div>
                            ))
                    }
                </Carousel>





                <div className="container-fluid">

                    <div className="section" ref={(section) => { this.Intro = section; }}>
                        <Waypoint
                            onEnter={({ previousPosition, currentPosition, event }) => {
                                this.setState({ first: true, visible: true });
                            }}
                            onLeave={({ previousPosition, currentPosition, event }) => {
                                this.setState({ visible: false });
                            }}

                        />

                        <div className={first ? "logo-blau fade-in-active animated" : "logo-blau animated"}><img src="../media/logo-servius.png" /></div>
                        <div className={first ? "intro-text fade-in-up" : 'intro-text'} >
                            {t("Servius es una empresa especializada en ofrecer servicios integrales dentro del ramo de la construcción. Realizamos todo tipo de trabajos tanto en edificios -residenciales y no residenciales- como en proyectos de obra civil. El éxito de nuestra larga trayectoria en el sector se debe a una filosofía de trabajo basada en el rigor profesional y el compromiso ético, siempre orientados a ofrecer un servicio premium.")}

                        </div>

                        <Waypoint onEnter={({ previousPosition, currentPosition, event }) => {
                            this.setState({ cubes: true });
                        }}
                        />

                        <h1 className={cubes ? "text-center fade-in-slow fade-in-active animated-slow servicio rem-0" : "text-center fade-in-slow animated-slow servicio rem-0"} id="section-intro">
                            {t("SERVICIOS")}
                        </h1>

                        {/*<div className="cub-ctn">

                            <div className={cubes ? "cub-in cub cub-active" : "cub cub-in"} onClick={() => scrollToComponent(this.Inmobiliaria, { offset: -10, align: 'top', duration: 1500 })}>
                                <p className="text-cub">{t("Sector inmobiliario B2B")}</p></div>
                            <div className={cubes ? "cub-em cub cub-active" : "cub cub-em"} onClick={() => scrollToComponent(this.Empresas, { offset: -10, align: 'top', duration: 1500 })}>
                                <p className="text-cub">{t("Empresas")}</p></div>
                            <div className={cubes ? "cub-ad cub cub-active" : "cub cub-ad"} onClick={() => scrollToComponent(this.Administracion, { offset: -10, align: 'top', duration: 1500 })}>
                                <p className="text-cub">{t("Administración Pública")}</p></div>
                            <div className={cubes ? "cub-par cub cub-active" : "cub cub-par"} onClick={() => scrollToComponent(this.Particulares, { offset: -10, align: 'top', duration: 1500 })}>
                                <p className="text-cub">{t("Particulares")}</p></div>
                        </div>
                   */}
                    </div>

                    <div className="cub-fix-ctn clearfix text-font-size">

                        <div className={cubes ? "cub-fix cub-a cub-active" : "cub-fix cub-a"} onClick={() => scrollToComponent(this.Inmobiliaria, { offset: -10, align: 'top', duration: 1500 })}>
                            <p className="text-font-size"> {t("Sector inmobiliario B2B")}</p>
                        </div>

                        <div className={cubes ? "cub-fix cub-em cub-b cub-active" : "cub-fix cub-em cub-b"}>

                            <p className="text-font-size"> {t("Empresas")}</p>
                        </div>

                        <div className={cubes ? "cub-fix cub-ad cub-c cub-active" : "cub-fix cub-ad cub-c"}>

                            <p className="text-font-size"> {t("Administración Pública")}</p>
                        </div>

                        <div className={cubes ? "cub-fix cub-par cub-d cub-active" : "cub-fix cub-par cub-d"}>

                            <p className="text-font-size"> {t("Particulares")}</p>
                        </div>

                    </div>

                    <Waypoint onEnter={({ previousPosition, currentPosition, event }) => {
                        this.setState({ second: true });
                    }}
                    />

                    <div className="section" ref={(section) => { this.Inmobiliaria = section; }}>

                        <div className="big-cube">
                            <img className={second ? "animated-slow fade-in-half inmob-rect" : "inmob-rect animated-slow"} />

                            <p className={second ? "pic-text animated-slow fade-in-active" : "pic-text animated-slow"}>{t("Sector inmobiliario B2B")}</p>
                        </div>
                        <div className={second ? "text text-destacados text-inmob text-destacados-active" : "text text-destacados text-inmob"}>
                            {t("Nuestro objetivo es ayudar a los distintos agentes del sector inmobiliario a incrementar el valor de sus productos. Pero sabemos que sólo un servicio premium puede marcar la diferencia en este sentido. Por eso en ServiUs solucionamos todo tipo de incidencias poniendo siempre énfasis en garantizar la máxima eficacia y calidad en los acabados, cuidando con sumo tacto a su vez la gestión y el trato personal con el usuario final.")}
                            <br /> <br />
                            {t("Poseemos una larga experiencia realizando trabajos de construcción y acondicionamiento de inmuebles y espacios comunitarios, así como servicios de post-venta y de mantenimiento. Si perteneces a una agencia inmobiliaria, un family office o eres promotor o administrador de fincas nuestro equipo de profesionales puede ayudarte a mejorar tus activos.")}

                        </div>

                        <div className="cub-fix-ctn clearfix">


                            <div className={second ? "cub-fix cub-1 cub-hover cub-active delay" : "cub-fix cub-1"}>
                                <img className="iconi iconi-petit" src="../media/inmob-icon-1.png"/>
                                <p className="text-movil">{t("Construcción de viviendas y oficinas")}</p>
                            </div>

                            <div className={second ? "cub-fix cub-2 cub-hover cub-active delay" : "cub-fix cub-2"}>
                                <img className="iconi" src="../media/inmob-icon-2.png" />
                                <p className="text-movil">{t("Mantenimientos y repasos integrales de post-venta")}</p></div>

                            <div className={second ? "cub-fix cub-3 cub-hover cub-active delay" : "cub-fix cub-3"}>
                                <img className="iconi" src="../media/inmob-icon-3.png" />
                                <p className="text-movil">{t("Realización de pisos muestra y reformas integrales")}</p></div>

                            <div className={second ? "cub-fix cub-4 cub-hover cub-active delay" : "cub-fix cub-4"}>
                                 <img className="iconi iconi-alto" src="../media/inmob-icon-4.png" />
                                <p className="text-movil">{t("Acondicionamiento de edificios y jardines")}</p></div>

                        </div>

                    </div>


                    <Waypoint onEnter={({ previousPosition, currentPosition, event }) => {
                        this.setState({ third: true });
                    }}
                    />

                    <div className="section" ref={(section) => { this.Empresas = section; }}>

                        <div className="big-cube">
                            <img className={third ? "animated-slow fade-in-half empresas-rect" : "animated-slow empresas-rect"} />
                            <p className={third ? "pic-text animated-slow fade-in-active" : "pic-text animated-slow"}>{t("Empresas")}</p>
                        </div>

                        <div className={third ? "text text-destacados text-empresas text-destacados-active" : "text text-destacados text-empresas"}>
                            {t("En Servius somos conscientes de que emprender es mucho más que una actividad profesional; es un proyecto de vida. Y por eso nos tomamos muy en serio tu negocio. La extensa variedad de necesidades de nuestros clientes nos ha convertido en una empresa ágil, capaz de garantizar el máximo servicio sea cual sea tu actividad.Tanto si estás iniciando un negocio, como si tu empresa está ya en funcionamiento o formas parte de una corporación consolidada podrás beneficiarte de nuestra experiencia para desarrollar tu proyecto.")}
                            <br /> <br />
                            {t("A lo largo de nuestra trayectoria hemos realizado todo tipo de trabajos para empresas de variados sectores; canalizaciones de gas a poblaciones para Gas Natural, campañas de luz para FECSA-ENDESA, construcción de locales para franquicias de distinta tipología, reformas de oficinas bancarias, instalaciones de todo tipo en oficinas, comercios y naves industriales. Siempre con estricto cumplimiento de plazos de entrega y ajuste a presupuesto inicial.")}

                        </div>

                        <div className="cub-fix-ctn clearfix">

                            <div className={third ? "cub-fix  cub-1 cub-hover cub-active" : "cub-fix cub-1"}>
                                <img className="iconi" src="../media/empresas-icon-1.png" />
                                <p className="text-movil"> {t("Reforma de locales comerciales, oficinas y edificios")}</p>
                            </div>

                            <div className={third ? "cub-fix cub-2 cub-hover cub-active" : "cub-fix cub-2"}>
                                <img className="iconi" src="../media/empresas-icon-2.png" />
                                <p className="text-movil"> {t("Construcción y acondicionamiento de naves industriales")}</p>
                            </div>

                            <div className={third ? "cub-fix cub-3 cub-hover cub-active" : "cub-fix cub-3"}>
                                <img className="iconi iconi-llum" src="../media/empresas-icon-3.png" />
                                <p className="text-movil"> {t("Montaje de equipamientos, mantenimientos y conducción de todo tipo de instalaciones")}</p>
                            </div>

                            <div className={third ? "cub-fix cub-4 cub-hover cub-active" : "cub-fix cub-4"}>
                                <img className="iconi" src="../media/empresas-icon-4.png" />
                                <p className="text-movil"> {t("Servicios de mantenimiento de inmuebles y exteriores")}</p>
                            </div>

                        </div>

                        <Waypoint onEnter={({ previousPosition, currentPosition, event }) => {
                            this.setState({ fourth: true });
                        }}
                        />


                        <div className="section" ref={(section) => { this.Administracion = section; }}>


                            <div className="big-cube">
                                <img className={fourth ? "animated-slow fade-in-half admin-rect" : "animated-slow admin-rect"} />
                                <p className={fourth ? "pic-text fade-in-active animated-slow" : "pic-text animated-slow"}> {t("Administración Pública")}</p>
                            </div>

                            <div className={fourth ? "text text-destacados text-admin text-destacados-active" : "text text-destacados text-admin"} >
                                {t("Nuestra clasificación y experiencia nos permite realizar trabajos en diversos campos de la obra pública: construcción y reformas de oficinas y locales públicos, mantenimientos, remodelaciones urbanas en exteriores o adecuación de espacios y viviendas sociales.")}
                                <br /> <br />
                                {t("Mantenemos una estrecha colaboración con diversos organismos públicos como la Generalitat de Catalunya, Regesa, Institut Català del Sol, Diputació de Barcelona, Infraestructures de Catalunya, Consell comarcal del Barcelonès, Consell comarcal del Maresme, Infraestructures ferroviaries de Catalunya, Reursa, Fundació privada pisos de lloguer, Copca, Regesa Aparcaments o diversos ayuntamientos.")}
                            </div>



                            <div className="cub-fix-ctn clearfix">


                                <div className={fourth ? "cub-fix cub-1 cub-hover cub-active delay" : "cub-fix cub-1"}>
                                    <img className="iconi" src="../media/admin-icon-1.png" />
                                    <p className="text-movil">{t("Construcción y reforma de locales y edificios públicos")}</p>
                                </div>

                                <div className={fourth ? "cub-fix  cub-2 cub-hover cub-active delay" : "cub-fix cub-2"}>
                                    <img className="iconi iconi-farola" src="../media/admin-icon-2.png" />
                                    <p className="text-movil">{t("Montaje de equipamientos, mantenimiento y conducción de todo tipo de instalaciones")}</p>
                                </div>

                                <div className={fourth ? "cub-fix cub-3 cub-hover cub-active delay" : "cub-fix cub-3"}>
                                    <img className="iconi" src="../media/admin-icon-3.png" />
                                    <p className="text-movil">{t("Servicios de mantenimiento de inmuebles y exteriores")}</p>
                                </div>

                                <div className={fourth ? "cub-fix cub-4 cub-hover cub-active delay" : "cub-fix cub-4"}>
                                    <img className="iconi iconi-grua" src="../media/admin-icon-4.png" />
                                    <p className="text-movil">{t("Urbanización y adecuación de espacios exteriores")}</p>
                                </div>

                            </div>

                        </div>

                        <Waypoint onEnter={({ previousPosition, currentPosition, event }) => {
                            this.setState({ fifth: true });
                        }}
                        />



                        <div className="section" ref={(section) => { this.Particulares = section; }}>

                            <div className="big-cube">
                                <img className={fourth ? "animated-slow fade-in-half particulares-rect" : "animated-slow particulares-rect"} />
                                <p className={fourth ? "pic-text fade-in-active animated-slow" : "pic-text animated-slow"}> {t("Particulares")}</p>
                            </div>

                            <div
                                className={
                                    fifth
                                        ? "text text-destacados text-particulares text-destacados-active"
                                        : "text text-destacados text-particulares"
                                }
                            >
                                {t("Cuando se trata de trabajar en un piso o una casa particular es fundamental entender que nos adentramos en el ámbito más íntimo de una persona o familia. Por ello en SERVIUS nos esforzamos ante todo en escuchar y comprender tus necesidades, en orientarte a partir de nuestra extensa experiencia y conjugar tus anhelos con las posibilidades del espacio del que dispones.")}
                                <br /> <br />
                                {t("Te ofrecemos un servicio “llaves en mano” premium, con el que disfrutarás de un trato absolutamente personalizado así como de la máxima garantía y calidad en los acabados. Nuestro objetivo es que no tengas que preocuparte por las obras y en definitiva que, en el futuro, cuando llegues a casa puedas disfrutar cómodamente de lo que realmente te importa; de tu hogar y de los tuyos. Y es que en ServiUs creemos que transformar un espacio en un hogar es mucho más que una simple reforma.")}
                            </div>

                            <div className="cub-fix-ctn clearfix">

                                <div className={fifth ? "cub-fix cub-1 cub-hover cub-active delay" : "cub-fix cub-1"}>
                                    <img className="iconi iconi-llum" src="../media/particulares-icon-1.png" />
                                    <p className="text-movil">{t("Diseño y realización de proyecto de obras")}</p>
                                </div>
                                <div
                                    className={
                                        fifth ? "cub-fix cub-2 cub-hover cub-active delay" : "cub-fix cub-2" }>
                                    <img className="iconi iconi-grande" src="../media/particulares-icon-2.png" />
                                    <p className="text-movil">
                                        {t("Reformas integrales de hogares y exteriores")}
                                    </p>
                                </div>
                                <div
                                    className={
                                        fifth ? "cub-fix cub-3 cub-hover cub-active delay" : "cub-fix cub-3"}>
                                    <img className="iconi" src="../media/particulares-icon-3.png" />
                                    <p className="text-movil">
                                        {t("Instalación y equipamiento de domótica, calefacción y aire acondicionado con cualquier tipo de energía")}
                                    </p>
                                </div>
                                <div
                                    className={
                                        fifth ? "cub-fix cub-4 cub-hover cub-active delay" : "cub-fix cub-4"}>
                                    <img className="iconi iconi-grande" src="../media/particulares-icon-4.png"/>
                                    <p className="text-movil">
                                        {t("Normalización y mantenimiento del suministro de agua, gas y electricidad")}
                                    </p>
                                </div>

                            </div>

                        </div>

                        <div className="section" ref={(section) => { this.Contacta = section; }}>

                            <Waypoint
                                onEnter={({ previousPosition, currentPosition, event }) => {
                                    this.setState({ sixth: true });
                                }}
                            />

                            <h1
                                className={
                                    sixth
                                        ? "contacta animated-slow fade-in-active"
                                        : "contacta animated-slow"
                                }
                            >
                                {t("Contacta")}
                            </h1>

                            <Media query="(min-width: 950px)">
                                {matches =>
                                    matches
                                        ? <div className="map-rem">
                                            <a
                                                className={
                                                    sixth
                                                        ? "map animated-slow fade-in-active"
                                                        : "map  animated-slow"
                                                }
                                                href="https://www.google.es/maps/place/Carrer+de+Lorena,+37,+08042+Barcelona/@41.4379735,2.1720502,17z/data=!4m5!3m4!1s0x12a4bd3c55cda5a3:0x95d42c4d1615362a!8m2!3d41.43807!4d2.1719"
                                                target="_blank"
                                            >
                                                <img src="../media/map.png" alt="" />
                                            </a>
                                        </div>
                                        : <div>
                                            <a
                                                className={
                                                    sixth
                                                        ? "map animated-slow fade-in-active"
                                                        : "map  animated-slow"
                                                }
                                                href="https://www.google.es/maps/place/Carrer+de+Lorena,+37,+08042+Barcelona/@41.4379735,2.1720502,17z/data=!4m5!3m4!1s0x12a4bd3c55cda5a3:0x95d42c4d1615362a!8m2!3d41.43807!4d2.1719"
                                                target="_blank"
                                            >
                                                <img src="../media/mapasm.png" alt="" />
                                            </a>
                                        </div>}
                            </Media>

                            <div
                                className={
                                    sixth
                                        ? "adress animated-slow fade-in-active fade-in-delay"
                                        : "adress  animated-slow"
                                }
                            >
                                Servicios a Usuarios S.A.<br />
                                c/Lorena 37-39 bajos, Barcelona 08042
                jordi@servius.es<br />
                                Movil 605 28 52 62
                Tel. 93 353 20 22
              </div>

                        </div>

                    </div>

                </div>
                {/* fi container-fluid*/}

                {/*<div id="scroll-top">TOP</div>*/}

            </div>
        );
    }
}

export default Home;
