import React, { Component } from "react";

class Footer extends Component {
  static propTypes = {};

  render() {
    return (
      <footer>
        <div id="footer" className="section" />
      </footer>
    );
  }
}

export default Footer;
