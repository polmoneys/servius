import React, { Component } from "react";
import { Link } from "react-router-dom";
// import { connect } from 'react-redux';
import { translate } from "react-i18next";

// @connect(({ app }) => ({ app }))
@translate()
class Header extends Component {
  static propTypes = {
    // app: PropTypes.object.isRequired
    t: PropTypes.func.isRequired
  };

  render() {
    const { isFixed, t } = this.props;

    return (
      <div
        id="top"
        className={isFixed ? "barra-nav barra-fixed" : "barra-nav barra-hidden"}
      >

        <div id="barra-burger" />

        <ul>
          <li><a id="conocenos-btn" href="#section-intro">{t("Conócenos")}</a></li>
          <li>
            <a id="inmobiliaria-btn" href="#section-inmobiliaria">
              {t("Inmobiliaria y Promotoras")}
            </a>
          </li>
          <li><a id="empresas-btn" href="#section-empresas">{t("Empresas")}</a></li>
          <li>
            <a id="administracion-btn" href="#section-administracion">
              {t("Administración Pública")}
            </a>
          </li>
          <li>
            <a id="particulares-btn" href="#section-particulares">
              {t("Particulares")}
            </a>
          </li>
          <li><a id="contacta-btn" href="#section-contacta">{t("Contacta")}</a></li>

        </ul>

      </div>
    );
  }
}

export default Header;
